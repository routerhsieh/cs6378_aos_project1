package proj1;

import java.util.*;
import java.util.concurrent.ConcurrentHashMap;

/**
 * Every method in this class should be thread-safe
 * @author router
 *
 */

public class ServerContext {
	private ParcelHandler handler = null;
	private int nodeNum;
	private HashMap<Integer, NodeInfo> nodeInfos = null;
	private final Map<UUID, Conversation> conversationStorage = new ConcurrentHashMap<UUID, Conversation>();
	volatile private boolean inTree = false;
	private String outputFilePath = null;
	ServerContext() {
	}

	ServerContext(ParcelHandler handler, int nodeNum,
			HashMap<Integer, NodeInfo> nodeInfos, String outputFilePath) {
		this.handler = handler;
		this.setCurrentNodeNum(nodeNum);
		this.setNodeInfos(nodeInfos);
		this.setOutputFilePath(outputFilePath);
	}

	public void setHandler(ParcelHandler handler) {
		this.handler = handler;
	}

	public ParcelHandler getHandler() {
		return this.handler;
	}

	public int getCurrentNodeNum() {
		return nodeNum;
	}

	public void setCurrentNodeNum(int nodeNum) {
		this.nodeNum = nodeNum;
	}
	
	public String getCurrentHost() {
		NodeInfo node = nodeInfos.get(nodeNum);
		return node.getHost();
	}
	
	public int getCurrentPort() {
		NodeInfo node = nodeInfos.get(nodeNum);
		return node.getPort();
	}

	public HashMap<Integer, NodeInfo> getNodeInfos() {
		return nodeInfos;
	}

	public void setNodeInfos(HashMap<Integer, NodeInfo> nodeInfos) {
		this.nodeInfos = nodeInfos;
	}

	public NodeInfo getCurrentNodeInfo() {
		NodeInfo node = nodeInfos.get(nodeNum);
		return node;
	}
	
	public NodeInfo getNodeInfo(int nodeNum) {
		return nodeInfos.get(nodeNum);
	}
	
	public String getNodeHost(int nodeNum) {
		return nodeInfos.get(nodeNum).getHost();
	}
	
	public int getNodePort(int nodeNum) {
		return nodeInfos.get(nodeNum).getPort();
	}

	public Set<Integer> getPhysicalNeighbors() {
		NodeInfo node = nodeInfos.get(nodeNum);
		Set<Integer> neighbors = node.getPhysicalNeighbors();
		return neighbors;
	}

	public Set<Integer> getLogicalNeighbors() {
		NodeInfo node = nodeInfos.get(nodeNum);
		Set<Integer> neighbors = node.getLogicalNeighbors();
		return neighbors;
	}
	
	public void setLogicalNeighbor(int neighborNodeId) {
		NodeInfo node = nodeInfos.get(nodeNum);
		Set<Integer> neighbors = node.getLogicalNeighbors();
		neighbors.add(neighborNodeId);
	}
	
	public void clearLogicalNeighbors() {
		NodeInfo node = nodeInfos.get(nodeNum);
		Set<Integer> neighbors = node.getLogicalNeighbors();
		neighbors.clear();
	}
	
	public synchronized boolean isInTree() {
		if (!this.inTree) {
			this.inTree = true;
			return false;
		}
		else {
			return true;
		}
	}
	

	public void setInTree(boolean inTree) {
		this.inTree = inTree;
	}

	public Conversation getConversation(UUID uuid) {
		return conversationStorage.get(uuid);
	}
	
	public Conversation newConversation(UUID uuid) {
		Conversation conversation = conversationStorage.get(uuid);
		if (conversation != null) {
			throw new IllegalArgumentException("Duplicate UUID: " + uuid) ;
		}
		else {
			conversation = new Conversation(uuid);
			conversationStorage.put(uuid, conversation);
			return conversation;
		}
	}
	
	public Conversation beginConversation() {
		Conversation conversation = new Conversation();
		conversationStorage.put(conversation.getUuid(), conversation);
		return conversation;
	}
	
	public void endConversation(UUID uuid) {
		conversationStorage.remove(uuid);
	}

	public String getOutputFilePath() {
		return outputFilePath;
	}

	public void setOutputFilePath(String outputFilePath) {
		this.outputFilePath = outputFilePath;
	}
}
