package proj1;
/**
 * 
 * @author router
 *
 */
import java.util.*; 

public class ParcelHandler implements Communicator.ParcelReceiver, ActionRegistry {
	private HashMap<ParcelType, Action> ActionMap = null;
	private Communicator communicator = null;
	
	ParcelHandler (Communicator communicator) {
		this.ActionMap = new HashMap<ParcelType, Action>();
		this.communicator = communicator;
	}
	
	public void setCommunicator(Communicator communicator) {
		this.communicator = communicator;
	}
	
	public Communicator getCommunicator() {
		return this.communicator;
	}
	
	public Action getAction(ParcelType type) {
		return ActionMap.get(type);
	}
	
	public void receive(Parcel parcel) {
		ParcelType type = parcel.getType();
		ParcelInfo info = parcel.getInfo();
		Message message = parcel.getMessage();
		
		Action action = ActionMap.get(type);
		action.does(info, message);
	}
	
	public void register(Action action) { 
		String[] typeStrings = action.getTypeStrings();
		for (String typeString : typeStrings) {
			ParcelType type = ParcelType.valueOf(typeString);
			ActionMap.put(type, action);
		}
	}
}
