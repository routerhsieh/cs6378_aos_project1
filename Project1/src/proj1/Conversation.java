package proj1;

import java.util.*;

public class Conversation {
	private final UUID uuid;
	private final boolean initiator;
	private int upperStream;
	@SuppressWarnings("unchecked")
	private Set<Integer> downStreams = Collections.EMPTY_SET;
	private Map<Integer, Acknowledgement> downStreamAcks = new HashMap<Integer, Acknowledgement>();
	private AckCallback callback;
	
	public static class Acknowledgement {
		boolean received;
		
		void setReceived(boolean received) {
			this.received = received;
		}
		
		boolean getReceived() {
			return this.received;
		}
	}
	
	Conversation () {
		this.uuid = UUID.randomUUID();
		this.initiator = true;
	}
	
	Conversation (UUID uuid) {
		this.uuid = uuid;
		this.initiator = false;
	}

	public UUID getUuid() {
		return uuid;
	}

	public boolean isInitiator() {
		return initiator;
	}

	public int getUpperStream() {
		return upperStream;
	}

	public void setUpperStream(int upperStream) {
		this.upperStream = upperStream;
	}

	public Set<Integer> getDownStreams() {
		return downStreams;
	}

	public void setDownStreams(Set<Integer> downStreams) {
		this.downStreams = downStreams;
		for (Integer downStream : downStreams) {
			this.downStreamAcks.put(downStream, new Acknowledgement());
		}
	}
	
	public boolean isAllAcknowledged() {
		boolean done = true;
		for (Integer downStream : downStreams) {
			done &= downStreamAcks.get(downStream).getReceived();
		}
		return done;
	}
	
	public void acknowledge(ParcelInfo info, Message message) {
		Acknowledgement ack = this.downStreamAcks.get(info.getFromNodeNum());
		ack.setReceived(true);
		this.callback.doAfterAcknowledged(info, message);
	}

	public AckCallback getCallback() {
		return callback;
	}

	public void setCallback(AckCallback callback) {
		this.callback = callback;
	}
}
