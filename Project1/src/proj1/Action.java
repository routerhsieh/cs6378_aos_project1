package proj1;
/**
 * 1. Provide a method called by parcelHandler
 * 2. Action types: Join, Send, Broadcast, Ack, NAck
 * 3. Interface will be implemented by APP layer
 * @author router
 *
 */
public interface Action {
	/**
	 * 
	 */
	public void does(ParcelInfo info, Message message);
	public String[] getTypeStrings();
}
