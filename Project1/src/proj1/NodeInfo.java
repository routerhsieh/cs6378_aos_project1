package proj1;
import java.util.*;
public class NodeInfo {
	private int nodeNum = 0;
    private String host = null;
    private int port = 0;
    private Set<Integer> physicalNeighbors = new HashSet<Integer>();
    private Set<Integer> logicalNeighbors = new HashSet<Integer>();


    NodeInfo (int nodeNum, String host, int port) {
        this.nodeNum = nodeNum;
        this.host = host;
        this.port = port;
    }

    public void setNodeNum(int nodeNum) {
        this.nodeNum = nodeNum;
    }

    public int getNodeNum() {
        return nodeNum;
    }

    public void setHost(String host) {
        this.host = host;
    }

    public String getHost() {
        return host;
    }

    public void setPort(int port) {
        this.port = port;
    }

    public int getPort() {
        return port;
    }

    public Set<Integer> getPhysicalNeighbors() {
        return this.physicalNeighbors;
    }
    
    public void setDirectNeighbor(int neighbor) {
        physicalNeighbors.add(neighbor);
    }

    public void setPhysicalNeighbors(Set<Integer> neighbors) {
        this.physicalNeighbors = neighbors;
    }

	public Set<Integer> getLogicalNeighbors() {
		return logicalNeighbors;
	}

	public void setLogicalNeighbors(Set<Integer> treeNeighbors) {
		this.logicalNeighbors = treeNeighbors;
	}   
	
	public void setLogicalNeighbor(int neighbor) {
		this.logicalNeighbors.add(neighbor);
	}
}
