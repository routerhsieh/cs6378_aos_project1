package proj1;

public class CmdAction implements Action {
	private ServerContext ctx;
	
	CmdAction (ServerContext ctx) {
		this.ctx = ctx;
	}
	
	@Override
	public void does(ParcelInfo info, Message message) {
		ParcelHandler handler = ctx.getHandler();
		int srcNodeNum = ctx.getCurrentNodeNum();
		ParcelType realType = message.getOriginalType();
		
		info = new ParcelInfo(srcNodeNum, srcNodeNum);
		message.setOriginalNodeNum(srcNodeNum);
		Conversation conversation = ctx.beginConversation();
		message.setConversationId(conversation.getUuid());
		Action action = handler.getAction(realType);
		action.does(info, message);
	}

	@Override
	public String[] getTypeStrings() {
		String[] typeStrings = {ParcelType.CMD.toString()};
		return typeStrings;
	}

}
