package proj1;

public interface ActionRegistry {
	public void register(Action action);
}
