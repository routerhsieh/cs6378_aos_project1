package proj1;

import java.util.*;

public class BroadcastAction implements Action {
	private ServerContext ctx;

	BroadcastAction(ServerContext ctx) {
		this.ctx = ctx;
	}
	
	private void sendAcknowledge(NodeInfo srcNode, NodeInfo dstNode, Communicator communicator, Message message, ParcelType outgoingType) {
		int srcNodeNum = srcNode.getNodeNum();
		
		int dstNodeNum = dstNode.getNodeNum();
		String dstHost = dstNode.getHost();
		int dstPort = dstNode.getPort();
		
		ParcelInfo info = new ParcelInfo(srcNodeNum, dstNodeNum);
		info.setDstHost(dstHost);
		info.setDstPort(dstPort);
		Parcel parcel = new Parcel(outgoingType, info, message);
		communicator.send(parcel);
	}
	
	private void doAfterAllDownStreamAcknowledged(Conversation conversation, ParcelType outgoingType, String actionString) {
		ParcelHandler handler = ctx.getHandler();
		Communicator communicator = handler.getCommunicator();

		int currentNodeNum = ctx.getCurrentNodeNum();
		NodeInfo currentNode = ctx.getCurrentNodeInfo();
		String currentHost = currentNode.getHost();
		int currentPort = currentNode.getPort();
		boolean notInitiator = !conversation.isInitiator();

		String doneMessage = String.format("%s done at Node %d(%s:%d)\n", actionString, currentNodeNum, currentHost, currentPort);
		NodeServer.messageOutput(ctx, doneMessage);;

		if (notInitiator) {
			int dstNodeNum = conversation.getUpperStream();
			String dstHost = ctx.getNodeHost(dstNodeNum);
			int dstPort = ctx.getNodePort(dstNodeNum);

			ParcelInfo ackInfo = new ParcelInfo(currentNodeNum, conversation.getUpperStream());
			ackInfo.setDstHost(dstHost);
			ackInfo.setDstPort(dstPort);

			String ackText = String.format("%s: reply from %s:%d\n", outgoingType.toString(), currentHost, currentPort);
			Message message = new Message();
			message.setText(ackText);
			message.setOriginalType(outgoingType);
			message.setConversationId(conversation.getUuid());;

			Parcel ackParcel = new Parcel(outgoingType, ackInfo,
					message);
			communicator.send(ackParcel);
		}
		
		switch (outgoingType) {
		case DACK:
			communicator.stopServer();
			break;
		case DJACK:
			ctx.clearLogicalNeighbors();
			ctx.setInTree(false);
			break;
		default:
			break;
		}
	}

	@Override
	public void does(ParcelInfo info, Message message) {
		/*
		 * 1. Print the received message. 
		 * 2. Get the ParcelHandler and tree neighbors from the server context(ctx) 
		 * 3. Get Communicator through ParcelHandler for send(). 
		 * 4. If the sender is the only tree neighbor, then send BACK message to sender. 
		 * 5. Otherwise, re-broadcast the message to tree neighbor, except the sender of the message
		 */
		HashMap<Integer, NodeInfo> nodeInfos = ctx.getNodeInfos();
		Set<Integer> logicalNeighbors = ctx.getLogicalNeighbors();
		ParcelHandler handler = ctx.getHandler();
		Communicator communicator = handler.getCommunicator();
		
		int upperStreamNodeNum = info.getFromNodeNum();
		NodeInfo upperStreamNode = nodeInfos.get(upperStreamNodeNum);
		String upperStreamHost = upperStreamNode.getHost();
		int upperStreamPort = upperStreamNode.getPort();
		
		NodeInfo currentNode = ctx.getCurrentNodeInfo();
		int currentNodeNum = currentNode.getNodeNum();
		String currentHost = currentNode.getHost();
		int currentPort = currentNode.getPort();
		
		if (logicalNeighbors.isEmpty()) {
			String emptyMessage = String.format("Node %d is not in spanning tree, please build it first\n", currentNodeNum);
			NodeServer.messageOutput(ctx, emptyMessage);
			return;
		}
		
		//Ok, the conversation might not be created, yet. But the tmp_conversation seems a little ugly..
		Conversation tmp_conversation = ctx.getConversation(message.getConversationId());
		if (tmp_conversation == null) {
			tmp_conversation = ctx.newConversation(message.getConversationId());
		}
		
		final ParcelType incomingType = message.getOriginalType();
		final ParcelType outgoingType;
		final String actionString, actionstring;
		switch (incomingType) {
		case BCAST:
			actionString = "Broadcast";
			actionstring = "broadcast";
			outgoingType = ParcelType.BACK;
			break;
		case DOWN:
			actionString = "Terminate";
			actionstring = "terminate";
			outgoingType = ParcelType.DACK;
			break;
		case DJOIN:
			actionString = "Disjoin";
			actionstring = "disjoin";
			outgoingType = ParcelType.DJACK;
			break;
		default:
			String errorMessage = String.format(
					"Error: Received Unknown ParcelType at Node %d!!\n",
					currentNodeNum);
			NodeServer.messageOutput(ctx, errorMessage);
			return;
		}
		
		final Conversation conversation = tmp_conversation;
		Set<Integer> downStreams = new HashSet<Integer>(logicalNeighbors);
		downStreams.remove(info.getFromNodeNum());
		conversation.setUpperStream(info.getFromNodeNum());
		conversation.setDownStreams(downStreams);
		boolean notInitiator = !conversation.isInitiator();
		
		if (notInitiator) {
			String receiveMessage = String.format("Node %d: Receiving message [%s] from node %d(%s:%d)\n", currentNodeNum, message.getText(), upperStreamNodeNum,upperStreamHost, upperStreamPort);
			NodeServer.messageOutput(ctx, receiveMessage);
		}
		else {
			String sendMessage = String.format("Node %d: Sending %s message [%s]\n", currentNodeNum, actionstring, message.getText());
			NodeServer.messageOutput(ctx, sendMessage);
		}

		if (logicalNeighbors.size() == 1 && notInitiator) { //handle leaf situation, because AckAction won't be trigger(no downStreams)
			sendAcknowledge(currentNode, upperStreamNode, communicator, message, outgoingType);
			String doneMessage = String.format("%s done at Node %d(%s:%d)\n", actionString, currentNodeNum, currentHost, currentPort);
			NodeServer.messageOutput(ctx, doneMessage);
			
			ctx.endConversation(message.getConversationId());
			
			switch (incomingType) {
			case DOWN:
				communicator.stopServer();
				break;
			case DJOIN:
				ctx.clearLogicalNeighbors();
				ctx.setInTree(false);
				break;
			default:
				break;
			}
		} else {
			for (Integer downStreamNodeId : conversation.getDownStreams()) {
				String downStreamHost = ctx.getNodeHost(downStreamNodeId);
				int downStreamPort = ctx.getNodePort(downStreamNodeId);
				
				ParcelInfo bcastInfo = new ParcelInfo(currentNodeNum, downStreamNodeId);
				bcastInfo.setDstHost(downStreamHost);
				bcastInfo.setDstPort(downStreamPort);
				
				Parcel parcel = new Parcel(message.getOriginalType(), bcastInfo, message);
				//final ParcelType messageType = message.getOriginalType();
				conversation.setCallback(new AckCallback.Default(){

					@Override
					public void doAfterAllAcknowledged() {
						doAfterAllDownStreamAcknowledged(conversation, outgoingType, actionString);
					} 
					
				});
				communicator.send(parcel);
			}
		}
	}

	@Override
	public String[] getTypeStrings() {
		String[] typeStrings = {ParcelType.BCAST.toString(), ParcelType.DOWN.toString(), ParcelType.DJOIN.toString()};
		return typeStrings;
	}

}
