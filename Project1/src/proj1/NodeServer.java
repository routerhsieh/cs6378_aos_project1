package proj1;

import java.io.*;
import java.text.*;
import java.util.*;

/**
 * 1. Entry point of whoal implementation
 * 2. Parse configuration file
 * 3. Module initialization
 * @author router
 *
 */
public class NodeServer {
	
	public static synchronized void messageOutput(ServerContext ctx, String originalMessage) {
		String outputFilePath = ctx.getOutputFilePath();
		DateFormat dateFormat = new SimpleDateFormat("HH:mm:ss:SS");
		String outputMessage = String.format("\n[%s]: %s", dateFormat.format(new Date()), originalMessage);
		File outputFile = new File(outputFilePath);
		if (!outputFile.exists()) {
			try {
				outputFile.createNewFile();
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
		
		try {
			Writer writer = new FileWriter(outputFile, true);
			writer.write(outputMessage);
			writer.flush();
			writer.close();
		} catch (IOException e) {
			e.printStackTrace();
		}
		
		System.out.print(outputMessage);
	}
	
	private static void buildNodeMapping(int nodeNum, String[] parameter, HashMap<Integer, NodeInfo> nodeInfos) {
		String host = parameter[0] + ".utdallas.edu";
		int port = Integer.parseInt(parameter[1]);
		NodeInfo info = new NodeInfo(nodeNum, host, port);
		Set<Integer> neighbors = info.getPhysicalNeighbors();
		for (int i = 2; i < parameter.length; i++) {
			neighbors.add(Integer.parseInt(parameter[i]));
		}
		nodeInfos.put(nodeNum, info);
	}
	
	private static void parseConfigFile(String file_path, HashMap<Integer, NodeInfo> nodeInfos) {
		File file = new File(file_path);
		int nodeNum = 1;
		Scanner scanner = null;
		try {
			scanner = new Scanner(file);

			while (scanner.hasNextLine()) {
				String line = scanner.nextLine();
				if (!line.isEmpty()) {
					char c = line.charAt(0);
					if (Character.isLetter(c)) {
						/* The '\s' means a string, so we need one more back slash('\') in front of '\s', 
						 * and '+' means one or more strings, that's a regular expression */
						String[] parameter = line.split("\\s+");
						buildNodeMapping(nodeNum, parameter, nodeInfos);
						nodeNum += 1;
					}
				}
			}//end of while loop
		}
		catch (FileNotFoundException ex) {
			System.out.printf("File %s is not found!!\n", file_path);
		}
		finally {
			if (scanner != null) {
				scanner.close();
			}
		}
	}
	
	public static void main (String[] args) {
		//TODO 
		//1. Parse Configruation
		//2. Prepare ServerContext
		//3. Moudle initialization
		/* We should pass the configuration file path and node number through command line argument */
		if (args.length < 3) {
			System.out.println("Usage: java proj1 config.txt NodeNumber output.log");
			return;
		}
		
		String intputFilePath = args[0];
		String outputFilePath = args[2];
		int nodeNumber = Integer.parseInt(args[1]);
		HashMap<Integer, NodeInfo> nodeInfos = new HashMap<Integer, NodeInfo>();
		parseConfigFile(intputFilePath, nodeInfos);
		NodeInfo node = nodeInfos.get(nodeNumber);
		int local_port = node.getPort();
		
		
		Communicator communicator = new CommunicatorImpl(local_port);
		ParcelHandler handler = new ParcelHandler(communicator);
		ServerContext ctx = new ServerContext(handler, nodeNumber, nodeInfos, outputFilePath);
		handler.register(new BroadcastAction(ctx));
		handler.register(new JoinAction(ctx));
		handler.register(new AcknowledgeAction(ctx));
		handler.register(new CmdAction(ctx));
		
		communicator.setReceiver(handler);
		
		String text = String.format("NodeServer %d at %s:%d start\n", nodeNumber, ctx.getCurrentHost(), ctx.getCurrentPort() );
		NodeServer.messageOutput(ctx, text);
	}
	
	/*
	class CommunicatorDummy implements Communicator{
		 private ParcelReceiver receiver;
		 public void send(Parcel parcel){
		  receiver.receive(parcel);
		 }
		 
		 public void subscribe(ParcelReceiver receiver){
		  this.receiver = receiver;
		 }
		}
	*/
}
